class Student

  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    raise "error" if has_conflict?(course)
    @courses << course unless @courses.include?(course)
    course.students << self unless course.students.include?(self)
  end

  def has_conflict?(new_course)
    @courses.any? do |old_course|
      new_course.conflicts_with?(old_course)
    end
  end

  def course_load
    course_load = Hash.new
    @courses.each do |course|
      if course_load.include?(course.department)
        course_load[course.department] += course.credits
      else
        course_load[course.department] = course.credits
      end
    end
    course_load
  end

end
